#include<stdio.h>
double income(double);
double cost(double);
double profit(double);
double attendees(double);
void showcase();

double attendees(double price){
        const int noa=120; //noa = number of attendees
        return noa-((price-15)/5*20);
}

double income(double price){
        return price*attendees(price);
}

double cost(double price){
        return 500+ 3*attendees(price);
}

double profit(double price){
        return income(price)-cost(price);
}

void showcase(){
        double Profit=0;
        for (int i = 5; i<=100;i+=5){
 	Profit = profit(i);
                printf("Rs %d \t Rs %.2f\n",i,Profit);
        }
}

int main(){
        printf("Relationship between the ticket price and the profit\n");
        printf("Price \t Profit\n");
        showcase();
        printf("So the highest profit = Rs.1260\n");
        printf("The most effective ticket price = Rs.25\n");
        return 0;

}

