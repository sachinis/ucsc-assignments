#include <stdio.h>
int main(){
	int matrix_1[10][10], matrix_2[10][10];
       	int matrix_sum[10][10]={0}, matrix_mul[10][10]={0};
	int r1,r2,c1,c2,i,j,k;
	printf("Enter number of rows and columns in the first matrix:\n");
	scanf("%d%d",&r1,&c1);
	printf("Enter number of rows and columns in the second matrix:\n");
	scanf("%d%d",&r2,&c2);
	if(r1!=r2 || c1!=c2){
		printf("Invalid addition");
	}
	else if(c1!=r2){
		printf("Matrix Multiplication is not possible");
	}
	else{
		printf("Enter elements of the first matrix:\n");
		for(i=0;i<r1;i++){
			for(j=0;j<c1;j++){
				scanf("%d", &matrix_1[i][j]);
			}
		}
		printf("Enter elements of the second matrix:\n");
		for(i=0;i<r2;i++){
			for(j=0;j<c2;j++){
				scanf("%d", &matrix_2[i][j]);
			}
		}
		//addition

		for(i=0;i<r1;i++){
			for(j=0;j<c1;j++){
				matrix_sum[i][j] = matrix_1[i][j] + matrix_2[i][j];
			}
		}
		
		printf("\nAddition of the matrix:\n");
		for(i=0;i<r1;i++){
			for(j=0;j<c1;j++){
				printf("%d ", matrix_sum[i][j]);
			}
			printf("\n");
		}
		//multiplication
		for(i=0;i<r1;i++){
			for(j=0;j<c2;j++){
				for(k=0;k<r2;k++){
					matrix_mul[i][j] += matrix_1[i][k]*matrix_2[k][j];
				}
			}
		}
		printf("\nMultiplication of the matrix:\n");
		for(i=0;i<r1;i++){
			for(j=0;j<c2;j++){
				printf("%d ", matrix_mul[i][j]);
			}
			printf("\n");
		}
	}
	return 0;
}


