#include <stdio.h>
#include <string.h>
 
int main()
{
    char arr[100];  
    int n,x;
 
    printf("Enter a sentence: ");
    fgets(arr,sizeof(arr),stdin);
    n=strlen(arr);
    
    for(int i=0;i<n/2;i++)  
    {
    	x=arr[i];
    	arr[i]=arr[n-i-1];
    	arr[n-i-1]=x;
 
 	}
 	 
	printf("Reversed sentence: %s\n",arr);
    
    return 0;
}
