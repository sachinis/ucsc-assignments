#include <stdio.h>
void pattern(int);
void raw(int);

int num_of_raws=1;
int n=0;


void pattern(int i)
{
    if (i>0){
        raw(num_of_raws);
        printf("\n");
	num_of_raws++;
	pattern(i-1);
}
}
 
void raw(int j)
{
    if (j>0){
    printf("%d",j);
    raw(j-1);
}
}
 

int main()
{
    printf("Enter the number of raws here = ");
    scanf("%d",&n);
    pattern(n);
    return 0;
}
